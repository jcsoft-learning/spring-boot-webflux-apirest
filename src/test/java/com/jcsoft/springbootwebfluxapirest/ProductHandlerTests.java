package com.jcsoft.springbootwebfluxapirest;

import com.jcsoft.springbootwebfluxapirest.model.document.Category;
import com.jcsoft.springbootwebfluxapirest.model.document.Product;
import com.jcsoft.springbootwebfluxapirest.model.service.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

/**
 * SpringBootTest permite trabajar con toodo el contexto de nuestra clase Application principal. Por consiguiente
 * utilizará la BD real.
 *
 * Estos Tests son para el handler
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductHandlerTests
{
    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private ProductService productService;
    @Value("${config.base.endpoint}")
    private String uri;

    @Test
    public void listTest()
    {
        webTestClient.get().uri(uri)
                .accept(MediaType.APPLICATION_JSON)
                //envia el request al endpoint y con el ResponseSpec que devuelve ya podemos hacer pruebas
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Product.class)
                //.hasSize(9)
                // aqui recibimos el response para poder manipularlo y verificar su data.
                .consumeWith(response -> {
                    List<Product> prods = response.getResponseBody();
                    assert prods != null;
                    prods.forEach(prod -> {
                        System.out.println(prod.getName());
                    });
                    Assertions.assertThat(prods.size() > 0).isTrue();
                    org.junit.jupiter.api.Assertions.assertTrue(prods.size() > 0);
                })
        ;
    }

    @Test
    public void showTest()
    {
        // ya que nuestra bd se llena cada vez que se ejecuta la aplicacion los ids generados son diferentes.
        // es necesario usar el block ya que en las pruebas unitarias no se puede trabajar con un suscribe al
        // necesitar la data del contexto de este test (tiene que ser sincrono).
        Product product = productService.findByName("TV Samsung LED").block();

        assert product != null;
        webTestClient.get().uri(uri + "/{id}", Collections.singletonMap("id", product.getId()))
                //.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                //envia el request al endpoint y con el ResponseSpec que devuelve ya podemos hacer pruebas
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Product.class)
                .consumeWith(response -> {
                    Product prod = response.getResponseBody();
                    Assertions.assertThat(prod.getId()).isNotEmpty();
                    Assertions.assertThat(prod.getId().length() > 0).isTrue();
                    Assertions.assertThat(prod.getName()).isEqualTo("TV Samsung LED");
                });
                /*.expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo("TV Samsung LED");*/
    }

    @Test
    public void createTest()
    {
        Category category = productService.findCategoryByName("Muebles").block();
        Product product = Product.builder().name("Mesa Comedor").price(100.00).category(category).build();
        webTestClient.post().uri(uri)
                //.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(product), Product.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo("Mesa Comedor");
    }

    @Test
    public void create2Test()
    {
        Category category = productService.findCategoryByName("Muebles").block();
        Product product = Product.builder().name("Mesa Comedor").price(100.00).category(category).build();
        webTestClient.post().uri(uri)
                //.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(product), Product.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Product.class)
                .consumeWith(response -> {
                    Product prod = response.getResponseBody();
                    Assertions.assertThat(prod.getId()).isNotEmpty();
                    Assertions.assertThat(prod.getName()).isEqualTo("Mesa Comedor");
                    Assertions.assertThat(prod.getCategory().getName()).isEqualTo("Muebles");
                });
    }

    @Test
    public void editTest() {
        Product product = productService.findByName("Camara Sony 4K").block();
        Category category = productService.findCategoryByName("Electrónico").block();

        // con el put debe de enviarse el objeto completo ya que si se manda elementos vacíos o no se envían también
        // los eliminará
        Product productEdited =
                Product.builder().name("Camara Sony 8K").price(product.getPrice()).category(product.getCategory()).build();

        webTestClient.put().uri(uri + "/{id}", Collections.singletonMap("id", product.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(productEdited), Product.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo("Camara Sony 8K");
    }

    @Test
    public void deleteTest() {
        Product product = productService.findByName("Sofa Cama").block();
        webTestClient.delete().uri(uri + "/{id}", Collections.singletonMap("id", product.getId()))
                .exchange()
                .expectStatus().isNoContent()
                .expectBody()
                .isEmpty();
    }
}
